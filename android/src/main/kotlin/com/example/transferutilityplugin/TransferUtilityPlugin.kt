package com.example.transferutilityplugin

import android.content.Context
import androidx.annotation.NonNull
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import java.io.File

/** TransferUtilityPlugin */
class TransferUtilityPlugin : FlutterPlugin, MethodCallHandler, EventChannel.StreamHandler {
    private var utility: TransferUtility? = null
    private var context: Context? = null
    private var methodChannel: MethodChannel? = null
    private var eventChannel: EventChannel? = null
    private var events: EventChannel.EventSink? = null

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        this.context = flutterPluginBinding.applicationContext
        methodChannel = MethodChannel(flutterPluginBinding.binaryMessenger, "transfer_utility_plugin_method_channel").apply {
            setMethodCallHandler(this@TransferUtilityPlugin)
        }
        eventChannel = EventChannel(flutterPluginBinding.binaryMessenger, "transfer_utility_plugin_event_channel").apply {
            setStreamHandler(this@TransferUtilityPlugin)
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        utility?.cancelAllWithType(TransferType.ANY)
        utility = null
        methodChannel?.setMethodCallHandler(null)
        methodChannel = null
        eventChannel?.setStreamHandler(null)
        eventChannel = null
        events = null
        context = null
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "build" -> {
                val region = call.argument<String>("region") ?: return result.success(false)
                val identityId = call.argument<String>("identityId") ?: return result.success(false)
                utility = TransferUtility.builder().apply {
                    context(context)
                    s3Client(
                        AmazonS3Client(
                            CognitoCachingCredentialsProvider(
                                context?.applicationContext, identityId, Regions.fromName(region)
                            ),
                            Region.getRegion(region)
                        )
                    )
                }.build()
                result.success(true)
            }
            "upload" -> {
                val bucket = call.argument<String>("bucket") ?: return result.success(false)
                val key = call.argument<String>("key") ?: return result.success(false)
                val path = call.argument<String>("path") ?: return result.success(false)
                val utility = utility ?: return result.success(false)
                utility.upload(bucket, key, File(path)).apply {
                    setTransferListener(object : TransferListener {
                        override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                            events?.success(bytesCurrent.toDouble() / bytesTotal.toDouble())
                        }

                        override fun onStateChanged(id: Int, state: TransferState?) {
                            if (state == TransferState.COMPLETED) {
                                result.success(true)
                            }
                        }

                        override fun onError(id: Int, ex: Exception?) {
                            result.success(false)
                        }
                    })
                }
            }
            "cancelAllWithType" -> {
                val utility = utility ?: return result.success(false)
                val type = call.arguments as? Int ?: return result.success(false)
                when (type) {
                    0 -> {
                        utility.cancelAllWithType(TransferType.UPLOAD)
                        result.success(true)
                    }
                    1 -> {
                        utility.cancelAllWithType(TransferType.DOWNLOAD)
                        result.success(true)
                    }
                    2 -> {
                        utility.cancelAllWithType(TransferType.ANY)
                        result.success(true)
                    }
                    else -> {
                        result.success(false)
                    }
                }
            }
            else -> {
                result.notImplemented()
            }
        }
    }

    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        this.events = events
    }

    override fun onCancel(arguments: Any?) {
        this.events = null
    }
}
