import "dart:io";

import "package:flutter/material.dart";
import "package:image_picker/image_picker.dart";
import "package:permission_handler/permission_handler.dart";
import "package:transfer_utility_plugin/constants.dart";
import "package:transfer_utility_plugin/transfer_utility_plugin.dart";

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Plugin example app"),
        ),
        body: Center(
          child: ElevatedButton(
            child: Text("Select image"),
            onPressed: () async {
              var status = Platform.isAndroid ? await Permission.storage.status : await Permission.photos.status;
              var shouldShowRequestRationale = Platform.isAndroid ? await Permission.storage.shouldShowRequestRationale : false;
              var result = status;
              if (result.isDenied) {
                result = Platform.isAndroid ? await Permission.storage.request() : await Permission.photos.request();
              }
              switch (result) {
                case PermissionStatus.granted:
                  final picker = ImagePicker();
                  final image = await picker.getImage(source: ImageSource.gallery);
                  if (image != null) {
                    final utility = await TransferUtilityPlugin.build(
                      "ap-northeast-1:0676f834-e001-4f56-a5e8-12abbba20bfc",
                      Region.apNortheast1,
                    );
                    final result = await utility?.upload("taiwannotes", "uploads/user/28/image", image.path, onProgressChanged: (progress) {
                      print(progress);
                    });
                    print(result);
                  }
                  break;
                case PermissionStatus.permanentlyDenied:
                  if (Platform.isAndroid && !shouldShowRequestRationale || status != PermissionStatus.denied) {
                    openAppSettings();
                  }
                  break;
                default:
              }
            },
          ),
        ),
      ),
    );
  }
}
