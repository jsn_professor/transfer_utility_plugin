import Flutter
import UIKit
import AWSS3

public class SwiftTransferUtilityPlugin: NSObject, FlutterPlugin, FlutterStreamHandler {
    static var eventChannel: FlutterEventChannel!
    static var methodChannel: FlutterMethodChannel!
    private var events: FlutterEventSink?

    public static func register(with registrar: FlutterPluginRegistrar) {
        let instance = SwiftTransferUtilityPlugin()
        eventChannel = FlutterEventChannel(name: "transfer_utility_plugin_event_channel", binaryMessenger: registrar.messenger())
        eventChannel.setStreamHandler(instance)
        methodChannel = FlutterMethodChannel(name: "transfer_utility_plugin_method_channel", binaryMessenger: registrar.messenger())
        registrar.addMethodCallDelegate(instance, channel: methodChannel)
    }

    public func detachFromEngine(for registrar: FlutterPluginRegistrar) {
        if let tasks = AWSS3TransferUtility.default().getUploadTasks().result as? [AWSS3TransferUtilityUploadTask] {
            for task in tasks {
                task.cancel()
            }
        }
        if let tasks = AWSS3TransferUtility.default().getDownloadTasks().result as? [AWSS3TransferUtilityDownloadTask] {
            for task in tasks {
                task.cancel()
            }
        }
        if let tasks = AWSS3TransferUtility.default().getMultiPartUploadTasks().result as? [AWSS3TransferUtilityMultiPartUploadTask] {
            for task in tasks {
                task.cancel()
            }
        }
        SwiftTransferUtilityPlugin.methodChannel = nil
        SwiftTransferUtilityPlugin.eventChannel = nil
        events = nil
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "build":
            guard let arguments = call.arguments as? [String: Any],
                  let regionType = (arguments["region"] as? String)?.regionType,
                  let identityId = arguments["identityId"] as? String,
                  let configuration = AWSServiceConfiguration(
                          region: regionType,
                          credentialsProvider: AWSCognitoCredentialsProvider(regionType: regionType, identityPoolId: identityId)) else {
                result(false)
                return
            }
            AWSServiceManager.default()?.defaultServiceConfiguration = configuration
            result(true)
        case "upload":
            guard let arguments = call.arguments as? [String: Any],
                  let bucket = arguments["bucket"] as? String,
                  let key = arguments["key"] as? String,
                  let path = arguments["path"] as? String else {
                result(false)
                return
            }
            let expression = AWSS3TransferUtilityUploadExpression()
            expression.progressBlock = {
                task, progress in
                self.events?(Double(progress.completedUnitCount) / Double(progress.totalUnitCount))
            }
            AWSS3TransferUtility.default().uploadFile(
                    URL(fileURLWithPath: path),
                    bucket: bucket,
                    key: key,
                    contentType: "*/*",
                    expression: expression) {
                task, error in
                if error == nil {
                    result(true)
                } else {
                    result(false)
                }
            }
            break
        case "cancelAllWithType":
            guard let type = call.arguments as? Int else {
                result(false)
                return
            }
            switch type {
            case 0:
                if let tasks = AWSS3TransferUtility.default().getUploadTasks().result as? [AWSS3TransferUtilityUploadTask] {
                    for task in tasks {
                        task.cancel()
                    }
                }
                result(true)
            case 1:
                if let tasks = AWSS3TransferUtility.default().getDownloadTasks().result as? [AWSS3TransferUtilityDownloadTask] {
                    for task in tasks {
                        task.cancel()
                    }
                }
                result(true)
            case 2:
                if let tasks = AWSS3TransferUtility.default().getUploadTasks().result as? [AWSS3TransferUtilityUploadTask] {
                    for task in tasks {
                        task.cancel()
                    }
                }
                if let tasks = AWSS3TransferUtility.default().getDownloadTasks().result as? [AWSS3TransferUtilityDownloadTask] {
                    for task in tasks {
                        task.cancel()
                    }
                }
                if let tasks = AWSS3TransferUtility.default().getMultiPartUploadTasks().result as? [AWSS3TransferUtilityMultiPartUploadTask] {
                    for task in tasks {
                        task.cancel()
                    }
                }
                result(true)
            default:
                result(false)
            }
        default:
            break
        }
    }

    public func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        self.events = events
        return nil
    }

    public func onCancel(withArguments arguments: Any?) -> FlutterError? {
        events = nil
        return nil
    }
}

private extension String {
    var regionType: AWSRegionType? {
        switch self {
        case "us-gov-west-1": return .USGovWest1
        case "us-gov-east-1": return .USGovEast1
        case "us-east-1": return .USEast1
        case "us-east-2": return .USEast2
        case "us-west-1": return .USWest1
        case "us-west-2": return .USWest2
        case "eu-west-1": return .EUWest1
        case "eu-west-2": return .EUWest2
        case "eu-west-3": return .EUWest3
        case "eu-central-1": return .EUCentral1
        case "eu-north-1": return .EUNorth1
        case "ap-east-1": return .APEast1
        case "ap-south-1": return .APSouth1
        case "ap-southeast-1": return .APSoutheast1
        case "ap-southeast-2": return .APSoutheast2
        case "ap-northeast-1": return .APNortheast1
        case "ap-northeast-2": return .APNortheast2
        case "sa-east-1": return .SAEast1
        case "ca-central-1": return .CACentral1
        case "cn-north-1": return .CNNorth1
        case "cn-northwest-1": return .CNNorthWest1
        case "me-south-1": return .MESouth1
        default:return nil
        }
    }
}
