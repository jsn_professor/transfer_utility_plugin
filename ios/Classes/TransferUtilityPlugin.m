#import "TransferUtilityPlugin.h"
#if __has_include(<transfer_utility_plugin/transfer_utility_plugin-Swift.h>)
#import <transfer_utility_plugin/transfer_utility_plugin-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "transfer_utility_plugin-Swift.h"
#endif

@implementation TransferUtilityPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftTransferUtilityPlugin registerWithRegistrar:registrar];
}
@end
