extension RegionExtension on Region {
  String get value {
    switch (this) {
      case Region.usGovWest1:
        return 'us-gov-west-1';
      case Region.usGovEast1:
        return 'us-gov-east-1';
      case Region.usEast1:
        return 'us-east-1';
      case Region.usEast2:
        return 'us-east-2';
      case Region.usWest1:
        return 'us-west-1';
      case Region.usWest2:
        return 'us-west-2';
      case Region.euWest1:
        return 'eu-west-1';
      case Region.euWest2:
        return 'eu-west-2';
      case Region.euWest3:
        return 'eu-west-3';
      case Region.euCentral1:
        return 'eu-central-1';
      case Region.euNorth1:
        return 'eu-north-1';
      case Region.apEast1:
        return 'ap-east-1';
      case Region.apSouth1:
        return 'ap-south-1';
      case Region.apSoutheast1:
        return 'ap-southeast-1';
      case Region.apSoutheast2:
        return 'ap-southeast-2';
      case Region.apNortheast1:
        return 'ap-northeast-1';
      case Region.apNortheast2:
        return 'ap-northeast-2';
      case Region.saEast1:
        return 'sa-east-1';
      case Region.caCentral1:
        return 'ca-central-1';
      case Region.cnNorth1:
        return 'cn-north-1';
      case Region.cnNorthwest1:
        return 'cn-northwest-1';
      case Region.meSouth1:
        return 'me-south-1';
    }
  }
}

enum Region {
  usGovWest1,
  usGovEast1,
  usEast1,
  usEast2,
  usWest1,
  usWest2,
  euWest1,
  euWest2,
  euWest3,
  euCentral1,
  euNorth1,
  apEast1,
  apSouth1,
  apSoutheast1,
  apSoutheast2,
  apNortheast1,
  apNortheast2,
  saEast1,
  caCentral1,
  cnNorth1,
  cnNorthwest1,
  meSouth1,
}

enum TransferType { upload, download, any }
