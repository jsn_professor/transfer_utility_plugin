import 'dart:async';

import 'package:flutter/services.dart';
import 'package:transfer_utility_plugin/constants.dart';

class TransferUtilityPlugin {
  static const MethodChannel _methodChannel = const MethodChannel('transfer_utility_plugin_method_channel');
  static const EventChannel _eventChannel = const EventChannel('transfer_utility_plugin_event_channel');

  static Future<TransferUtilityPlugin?> build(String identityId, Region region) async {
    final result = await _methodChannel.invokeMethod(
          'build',
          {
            'identityId': identityId,
            'region': region.value,
          },
        ) ??
        false;
    return result ? TransferUtilityPlugin() : null;
  }

  Future<bool> upload(String bucket, String key, String path, {Function(double progress)? onProgressChanged}) async {
    var subscription = onProgressChanged != null ? _eventChannel.receiveBroadcastStream().map<double>((value) => value).listen(onProgressChanged) : null;
    final result = await _methodChannel.invokeMethod(
          'upload',
          {
            'bucket': bucket,
            'key': key,
            'path': path,
          },
        ) ??
        false;
    subscription?.cancel();
    return result;
  }

  Future<bool> cancelAllWithType(TransferType type) async {
    return await _methodChannel.invokeMethod('cancelAllWithType', type.index) ?? false;
  }
}
